"""Program outputs the time in minutes until the next bus departure.
Steven Cacner

Arguments:
bus_route (str): Name of bus route.
bus_stop (str): Name of bus stop.
bus_direction (str): Going direction of the bus.

Output:
Time in minutes until the next bus departure at the inputted bus stop going in
the inputted direction on the inputted route.

Exit Codes:
0 - Successful program completion.
1 - Bad number of arguments error.
2 - Error while receiving JSON response from api.
3 - Invalid argument.
4 - Bad JSON data.
5 - Error while converting or calculating Departure Time.
"""

# Import necessary libraries.
import datetime  # Used to get time of next departure.
import json  # Used to get information from api.
import math  # Used to get a whole number for the output.
import re  # Used to extract POSIX time from JSON Date information.
import sys  # Used to retrieve program arguments and return exit codes.
import urllib.request  # Used to access api.
import urllib.error  # Used to detect errors when accessing api.


def json_request(url):
    """Get JSON response from inputted URL.

    Parameters:
    url (str): inputted URL.

    Return (list): JSON response as a list of dictionaries.
    """
    try:  # urllib can raise various errors, check for them.
        with urllib.request.urlopen(url) as url:
            data = json.load(url)
            return data
    except urllib.error.ContentTooShortError:
        print('Error: Bad response from api.')
        sys.exit(2)
    except urllib.error.HTTPError:
        print('Error: Bad response from api.')
        sys.exit(2)
    except urllib.error.URLError:
        print('Error: Bad response from api.')
        sys.exit(2)
    except json.JSONDecodeError:
        print('Error: Bad response from api.')
        sys.exit(2)


def get_num(mode, values, value):
    """Get the mode's number of inputted value from list of values.

    Parameters:
    mode (int): "Route", "Direction", or "Stop"
    values (list): Applicable values for desired mode.
    value (str): Inputted value.

    Return (str): Value number.
    """

    # If in Route mode, make the search term 'Description' and return term
    # 'Route', else make the search term 'Text' and return term 'Value'.
    if mode == 'Route':
        search_term = 'Description'
        return_term = 'Route'
    else:
        search_term = 'Text'
        return_term = 'Value'

    # Make sure values do exist before continuing.
    if len(values) < 1:
        print('Error: No {}s exist.'.format(mode))
        sys.exit(4)

    # Make sure dictionary values exist before continuing.
    if search_term not in values[0] or return_term not in values[0]:
        print('Error: Bad data from api.')
        sys.exit(4)

    # If in Direction mode, make 'value' all uppercase to match api output.
    if mode == 'Direction':
        value = value.upper()

    # Check if 'value' matches at least part of a list value in 'values'.
    try:
        value_num = next(item for item in values if value in item[search_term])
    # This error is called if end of list is reached without finding a match.
    except StopIteration:
        print('Error: {} "{}" invalid.'.format(mode, value))
        sys.exit(3)

    # Return 'value_num', the inputted value's number.
    return value_num[return_term]


def get_time(info):
    """Get the time until next departure from information of next departure.

    Parameters:
    info (dict): Next departure information

    Return (int): Number of minutes until next departure.
    """

    # Get 'DepartureTime' dictionary value from argument. If value doesn't
    # exist, error out.
    if 'DepartureTime' not in info:
        print('Error: Bad data from api.')
        sys.exit(4)
    depart_time = info['DepartureTime']

    # Convert Date JSON value to datetime format.
    depart_time = re.split('[()]', depart_time)[1][:10]
    try:  # fromtimestamp() can raise Overflow and OSError, check.
        depart_time = datetime.datetime.fromtimestamp(int(depart_time))
    except OverflowError:
        print('Error: Bad datetime conversion.')
        sys.exit(5)
    except OSError:
        print('Error: Bad datetime conversion.')
        sys.exit(5)

    # Get present time in datetime format.
    current_time = datetime.datetime.now()

    # Find difference between departure time and current time.
    arrive_time = depart_time - current_time

    # Take the ceiling of the difference in minutes
    try:  # timedelta() can raise an OverflowError, check.
        arrive_time_min = arrive_time / datetime.timedelta(minutes=1)
    except OverflowError:
        print('Error: Bad datetime calculation.')
        sys.exit(5)
    arrive_time_min = math.ceil(arrive_time_min)

    # Return 'arrive_time_min', the number of minutes until the next departure.
    return arrive_time_min


def main(route, stop, direction):
    """Main function of program.

    Parameters:
    route (str): a substring of the bus route name which is only in one bus
        route.
    stop (str): a substring of the bus stop name which is only in one bus stop
        on that route
    direction (str): “north”, “east”, “west”, or “south”
    """

    # Instantiate remaining necessary variables.
    base_url = "http://svc.metrotransit.org/NexTrip/"
    json_format = "?format=json"

    # Find all routes for present day.
    routes = json_request(base_url + "Routes" + json_format)

    # Get route number of desired route.
    route_num = get_num("Route", routes, route)

    # Find all directions for route.
    directions = json_request(base_url + "Directions/{}".format(route_num)
                              + json_format)

    # Get direction number of desired direction.
    direction_num = get_num("Direction", directions, direction)

    # Find all stops for direction and route.
    stops = json_request(base_url
                         + "Stops/{}/{}".format(route_num, direction_num)
                         + json_format)

    # Get stop number of desired stop.
    stop_num = get_num("Stop", stops, stop)

    # Find all departure times for stop, direction, and route.
    times = json_request(base_url + "{}/{}/{}".format(route_num, direction_num,
                                                      stop_num) + json_format)

    # Continue if there is another departure time for the present day.
    if len(times) > 0:
        # Get the next departure time in minutes.
        time = get_time(times[0])

        # Define unit of time.
        unit = "Minute"

        # Pluralize unit of time is not equal to 1.
        if time != 1:
            unit += "s"

        # Print it.
        print("{} {}".format(time, unit))


if __name__ == '__main__':
    # Make sure three arguments were included with the program call.
    if len(sys.argv) != 4:
        # Print an error message if there are not three arguments.
        print("This program outputs the time in minutes until the next bus "
              + "departure.\n\n"
              + "Program call:\n"
              + "  python nextbus.py [bus_route] [bus_stop] [bus_direction]\n"
              + "    bus_route - Name of bus route.\n"
              + "    bus_stop - Name of bus stop.\n"
              + "    bus_direction - Going direction of the bus.")
        sys.exit(1)

    main(sys.argv[1], sys.argv[2], sys.argv[3])
