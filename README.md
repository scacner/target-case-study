# Case Study - API Consumption

In Python 3, a program was written to output the length of time in minutes until the next bus on "bus_route" leaving from "bus_stop" going "bus_direction" departs. The information about the bus was pulled from the [MetroTransit API](http://svc.metrotransit.org).

## Getting Started

These instructions will explain how to get the program running on a local machine.

### Prerequisites

The following program needs to be installed on the local machine.


* [Python 3.6.5](https://www.python.org/downloads/release/python-365/) - The latest release of Python.

## Running
To run the program, invoke the command:

```
$ python nextbus.py [bus_route] [bus_stop] [bus_direction]
```

where argument:

* ```bus_route``` is the name of the bus route.
* ```bus_stop``` is the name of the bus stop.
* ```bus_direction``` is the going direction of the bus.

## Testing

Some example tests showing various situations and route, stop, and direction groupings are shown below.

### Test 1 - Departure Time > One Minute

This test shows the program execution and output of an inputted bus route, stop, and direction that results in a length of time until the next bus departure. The test ran on 4/9/2018 at 11:46pm EDT.

```
$ python nextbus.py "METRO Blue Line" "Target Field Station Platform 1" "south"
33 Minutes
```

### Test 2 - Departure Time = One Minute

This test shows the program execution and output of an inputted bus route, stop, and direction  that results in a length of time (spoiler alert: one minute) until the next bus departure. The test ran on 4/11/2018 at 6:17pm EDT.

```
$ python nextbus.py "METRO Green Line" "Target Field Station Platform 2" "east"
1 Minute
```

### Test 3 - No More Departures

This test shows the program execution of an inputted bus route, stop, and direction, which results in no output of a length of time until departure due to the last bus having already left for the day. The test ran on 4/10/2018 at 12:49pm EDT.

```
$ python nextbus.py "Express - Target - Hwy 252 and 73rd Av P&R - Mpls" "Target North Campus Building F" "north"
```

## Author

* **Steven T. Cacner, Jr.** - [LinkedIn](https://www.linkedin.com/in/stevencacner)